public class WildCat {

    // TODO Complete me!
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    //Constructor
    public WildCat(String name, double weight, double length) {
      this.name = name;
      this.weight = weight;
      this.length = length;
    }

    // Rumus Bmi Standar
    public double computeMassIndex() {
        return weight/((length/100)*(length/100));
    }
}
