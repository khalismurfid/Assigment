import java.util.Scanner;
import java.util.LinkedList;
import java.text.DecimalFormat;

public class A1Station {
    private static LinkedList <TrainCar> track = new LinkedList <TrainCar>(); // Ini buat LinkedListnya
    private static final double THRESHOLD = 250; // in kilograms
    private static TrainCar latest; // Ini buat nyimpen kereta yang paling baru diinisiasi
    private static DecimalFormat df2 = new DecimalFormat("#.##"); // ini buat bikin double jadi dua angka dibelakang koma cuz test casenya outputnya 2 angka dibelakang koma

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double weight =0; // nah ini kita inisiasi dulu weightnya soalnya kan nanti mau dicek tuh beratnya melebihi THRESHOLD atau engga
        int jumlah = Integer.parseInt(scan.nextLine());
        if(jumlah==0){ // ini mencegah biar kalau misalnya inputnya 0, si meluncur yang di line 39 ga jalan.kalau jalan nanti errror meski gaada testcasenya
          return;
        }
        for(int a = 0; a<jumlah; a++){
          String [] properties = scan.next().split(","); // ini kita pisa pake koma kaya yang bingo kemaren
          if(!track.isEmpty()){ // Nah ini tuh buat ngecek track yaitu LinkedList yang dibuat tadi.. cek kalau dia gakosong
            latest = track.getFirst(); // kalau gak kosong, berarti kan ada isinya tuh. nah berarti latestnya kita ganti pake isinya yang paling pertama makanya get First
            track.addFirst(new TrainCar(new WildCat(properties[0],Double.parseDouble(properties[1]),Double.parseDouble(properties[2])), latest)); // ini tuh maksudnya adalah kita masukin ke linkedlist urutan pertama object TrainCar yang berparameter (WildCat yang diambil dari input, sama nextnya yaitu latest)
            weight=track.getFirst().computeTotalWeight();// nah weight yang tadi kita ganti jadi computeTotalWeightnya sih traincar paling baru yang bisa kita ambil pake track.getFirst
            if (weight>=THRESHOLD){// ini kalau udah kelebihan beratnya langsung meluncur
            Meluncur();
            }
          }
          else{// bedanya ini dari yang tadi sebenernya cuman di kalau ini, kan berarti linkedlistnya kosong yang berarti ini TrainCar pertama yang bakal masuk, makanya dia inisiasi traincarnya parameternya cuman(WildCat)
            track.addFirst(new TrainCar(new WildCat(properties[0],Double.parseDouble(properties[1]),Double.parseDouble(properties[2]))));
            weight=track.getFirst().computeTotalWeight();
            if (weight>=THRESHOLD){
            Meluncur();
            }
          }
        }
        if(!track.isEmpty()){
        Meluncur(); // ini tuh buat kalau misalnya loopnya dah selese dan misalkany masih ada train car yang belom meluncur, nah kita luncurin gitu
      }
    }

    public static void Meluncur(){ // meluncur ini yah isinya sudah jelas kok bisa dong ngerti
        double averageMassIndex = track.getFirst().computeTotalMassIndex()/track.size();
        String category = "";
        if (averageMassIndex<18.5){
          category = "underweight";
        }
        else if(averageMassIndex>=18.5 && averageMassIndex < 25){
          category = "normal";
          }
        else if(averageMassIndex>=25 && averageMassIndex < 30){
          category = "overweight";
        }
        else if(averageMassIndex>=30){
          category = "obese";
        }
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        track.getFirst().printCar();
        System.out.println("Average mass index of all cats: "+ df2.format(averageMassIndex));
        System.out.println("In average, the cats in the train are *"+category+"*");
        track.clear();
        }
}
