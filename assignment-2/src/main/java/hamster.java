import java.util.ArrayList;
public class hamster extends animals{
  int length;
  public static ArrayList<hamster> list = new ArrayList <hamster>();

  public hamster(String name, int length){
    super(name,length);
    list.add(this);
  }

  public void gnaw(){
    System.out.println(name+ " makes a voice: Ngkkrit.. Ngkkrrriiit");
    System.out.println("Back to the office!");
  }

  public void run(){
    System.out.println(name+ " makes a voice: Trrr... Trrr...");
    System.out.println("Back to the office!\n");
  }

}
