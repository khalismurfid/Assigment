public class animals{

  String name;
  int length;
  boolean isIndoor = true;
  public animals(String name, int length){
    this.name = name;
    this.length = length;
  }
  public String getName(){
    return name;
  }
  public int getLength(){
    return length;
  }
  public boolean getIsIndoor(){
    return isIndoor;
  }
}
