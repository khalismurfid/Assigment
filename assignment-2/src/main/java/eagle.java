import java.util.ArrayList;
public class eagle extends animals{
  int length;
  public static ArrayList<eagle> list = new ArrayList <eagle>();

  public eagle(String name, int length){
    super(name,length);
    isIndoor = false;
    list.add(this);
  }

  public void fly(){
    System.out.println(name+ " makes a voice: Kwaakk....");
    System.out.println("You hurt!");
    System.out.println("Back to the office!\n");
  }

}
