import java.util.ArrayList;
public class lion extends animals{
  int length;
  public static ArrayList<lion> list = new ArrayList <lion>();

  public lion(String name, int length){
    super(name,length);
    isIndoor = false;
    list.add(this);
  }


  public void hunt(){
    System.out.println(name+ " makes a voice: Err....");
    System.out.println("Back to the office!\n");
  }

  public void groomed(){
    System.out.println(name+ " makes a voice: Hauhhmm!");
    System.out.println("Back to the office!\n");
  }

  public void disturbed(){
    System.out.println(name+" makes a voice: HAUHHMM!");
    System.out.println("Back to the office!\n");
  }

}
