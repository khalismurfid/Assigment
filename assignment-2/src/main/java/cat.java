import java.util.Random;
import java.util.ArrayList;
public class cat extends animals{
  int length;
  public static ArrayList<cat> list = new ArrayList <cat>();

  public cat(String name, int length){
    super(name,length);
    list.add(this);
  }

  public void brushed(){
    System.out.printf("Time to clean %s fur\n",name);
    System.out.println(name+ " makes a voice: Nyaaan...");
    System.out.println("Back to the office!");
  }

  public void cuddled(){
    String[] voice= {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    Random rand = new Random();
    int value = rand.nextInt(4);
    System.out.println(name+ " makes a voice: "+voice[value]);
    System.out.println("Back to the office!\n");
  }



}
