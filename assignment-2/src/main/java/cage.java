public class cage{
  String type;
  String place;
  String animal;
  String name;
  int length,width,animalLength;
  animals animal1;
  cat cats;
  lion lions;
  eagle eagles;
  parrot parrots;
  hamster hamsters;

  public cage(cat cats){
    this.cats = cats;
    animal = "cat";
    name = cats.getName();
    placement(cats.getLength(),cats.getIsIndoor());
    animalLength = cats.getLength();
  }
  public cage(lion lions){
    this.lions = lions;
    animal = "lion";
    name = lions.getName();
    placement(lions.getLength(),lions.getIsIndoor());
    animalLength = lions.getLength();
  }
  public cage(eagle eagles){
    this.eagles = eagles;
    animal = "lion";
    name = eagles.getName();
    placement(eagles.getLength(),eagles.getIsIndoor());
    animalLength = eagles.getLength();
  }
  public cage(parrot parrots){
    this.parrots = parrots;
    animal = "parrot";
    name = parrots.getName();
    placement(parrots.getLength(),parrots.getIsIndoor());
    animalLength = parrots.getLength();
  }
  public cage(hamster hamsters){
    this.hamsters = hamsters;
    animal = "hamster";
    name = hamsters.getName();
    placement(hamsters.getLength(),hamsters.getIsIndoor());
    animalLength = hamsters.getLength();
  }

  private void placement(int length,boolean isIndoor){
    if(isIndoor){
      place = "Indoor";
      if(length<=45){
        type = "A";
        length = 60;
        width = 60;
      }
      else if(45<length&&length<60){
        type = "B";
        length = 60;
        width = 90;
      }
      else{
        type = "C";
        length = 60;
        width = 120;
      }
    }
    else{
      place = "Outdoor";
      if(length<=75){
        type = "A";
        length = 120;
        width = 120;
      }
      else if(75<length&&length<90){
        type = "B";
        length = 120;
        width = 150;
      }
      else{
        type = "C";
        length = 120;
        width = 180;
      }
    }
  }
}
