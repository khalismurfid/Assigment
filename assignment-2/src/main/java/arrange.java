import  java.util.*;
import java.lang.*;
public class arrange{
  public static void rearrange(cage[][] cages){
    if(cages[0]==null && cages[1] == null && cages[2]==null){
      return;
    }
    System.out.print("\n\nAfter rearrangement...");
		for(int i = 2; i > 0; i--){
			cage[] temp = cages[i];
			cages[i] = cages[i-1];
			cages[i-1] = temp;
		}
    for(int j = 0; j < cages.length; j++){
      if(cages[j]==null){
        continue;
      }
      for(int i = 0; i < cages[j].length / 2; i++) {
        if(cages[i][j]==null){
          continue;
        }
          cage temp = cages[j][i];
          cages[j][i] = cages[j][cages[j].length - i - 1];
          cages[j][cages[j].length - i - 1] = temp;
        }
    }
		for(int i = 2; i >= 0; i--){
			System.out.print("\nlevel " + (i+1) + ": ");
      if(cages[i]==null){
        continue;
      }
			for(int j = 0; j < cages[i].length; j++){
				if(cages[i][j] == null){
					continue;
				}
				System.out.print(cages[i][j].name + " (" + cages[i][j].animalLength + " - " + cages[i][j].type + "), ");
			}
		}
		System.out.print("\n");
  }
  public static void cagesarrangecat(cage[][] cages, ArrayList<cat> cats){
    if(!cats.isEmpty()){
      int counter=0;
      int basic = cats.size()/3;
      int rem = cats.size()%3;
      if(cats.size()<3){
        if(cats.size()==2){
          cages[0]= new cage[1];cages[1]=new cage[1];
          cages[0][0] = new cage(cats.get(0));
          cages[1][0] = new cage(cats.get(1));
        }
        if(cats.size()==1){
          cages[0]=new cage[1];
          cages[0][0] = new cage(cats.get(0));
        }
      }
      else{
        if(rem==0){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic];
        }
        else if(rem==1){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic+1];
        }
        else{
          cages[0]= new cage[basic];
          cages[1]= new cage[basic+1];
          cages[2]= new cage[basic+1];
        }
        for(int a =0; a<3;a++){
          for(int b =0;b<basic;b++){
            cages[a][b]=new cage(cats.get(counter));
            counter+=1;
          }
        }
        if(rem==1){
          cages[2][basic] = new cage(cats.get(counter));
        }
        if(rem==2){
          cages[2][basic] = new cage(cats.get(counter));
          cages[1][basic] = new cage(cats.get(counter+1));
        }
      }
      System.out.println("Cage arrangement:");
      System.out.println("location: indoor");
      System.out.print("level 3: ");
      if(cages[2]!=null){
        for(int a =0; a<cages[2].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[2][a].name,cages[2][a].animalLength,cages[2][a].type);
        }
      }
      System.out.print("\nlevel 2: ");
      if(cages[1]!=null){
        for(int a =0; a<cages[1].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[1][a].name,cages[1][a].animalLength,cages[1][a].type);
        }
      }
      System.out.print("\nlevel 1: ");
      if(cages[0]!=null){
        for(int a =0; a<cages[0].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[0][a].name,cages[0][a].animalLength,cages[0][a].type);
        }
      }
    }
  }
    public static void cagesarrangelion(cage[][] cages, ArrayList<lion> lions){
      if(!lions.isEmpty()){
        int counter=0;
        int basic = lions.size()/3;
        int rem = lions.size()%3;
        if(lions.size()<3){
          if(lions.size()==2){
            cages[0]= new cage[1];cages[1]=new cage[1];
            cages[0][0] = new cage(lions.get(0));
            cages[1][0] = new cage(lions.get(1));
          }
          if(lions.size()==1){
            cages[0]=new cage[1];
            cages[0][0] = new cage(lions.get(0));
          }
        }
        else{
          if(rem==0){
            cages[0]= new cage[basic];
            cages[1]= new cage[basic];
            cages[2]= new cage[basic];
          }
          else if(rem==1){
            cages[0]= new cage[basic];
            cages[1]= new cage[basic];
            cages[2]= new cage[basic+1];
          }
          else{
            cages[0]= new cage[basic];
            cages[1]= new cage[basic+1];
            cages[2]= new cage[basic+1];
          }
          for(int a =0; a<3;a++){
            for(int b =0;b<basic;b++){
              cages[a][b]=new cage(lions.get(counter));
              counter+=1;
            }
          }
          if(rem==1){
            cages[2][basic] = new cage(lions.get(counter));
          }
          if(rem==2){
            cages[2][basic] = new cage(lions.get(counter));
            cages[1][basic] = new cage(lions.get(counter+1));
          }
        }
        System.out.println("\nCage arrangement:");
        System.out.println("location: outdoor");
        System.out.print("level 3: ");
        if(cages[2]!=null){
          for(int a =0; a<cages[2].length;a++){
            System.out.printf( "%s (%d - %s), ",cages[2][a].name,cages[2][a].animalLength,cages[2][a].type);
          }
        }
        System.out.print("\nlevel 2: ");
        if(cages[1]!=null){
          for(int a =0; a<cages[1].length;a++){
            System.out.printf( "%s (%d - %s), ",cages[1][a].name,cages[1][a].animalLength,cages[1][a].type);
          }
        }
        System.out.print("\nlevel 1: ");
        if(cages[0]!=null){
          for(int a =0; a<cages[0].length;a++){
            System.out.printf( "%s (%d - %s), ",cages[0][a].name,cages[0][a].animalLength,cages[0][a].type);
          }
        }
      }
}

  public static void cagesarrangeeagle(cage[][] cages, ArrayList<eagle> eagles){
    if(!eagles.isEmpty()){
      int counter=0;
      int basic = eagles.size()/3;
      int rem = eagles.size()%3;
      if(eagles.size()<3){
        if(eagles.size()==2){
          cages[0]= new cage[1];cages[1]=new cage[1];
          cages[0][0] = new cage(eagles.get(0));
          cages[1][0] = new cage(eagles.get(1));
        }
        if(eagles.size()==1){
          cages[0]=new cage[1];
          cages[0][0] = new cage(eagles.get(0));
        }
      }
      else{
        if(rem==0){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic];
        }
        else if(rem==1){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic+1];
        }
        else{
          cages[0]= new cage[basic];
          cages[1]= new cage[basic+1];
          cages[2]= new cage[basic+1];
        }
        for(int a =0; a<3;a++){
          for(int b =0;b<basic;b++){
            cages[a][b]=new cage(eagles.get(counter));
            counter+=1;
          }
        }
        if(rem==1){
          cages[2][basic] = new cage(eagles.get(counter));
        }
        if(rem==2){
          cages[2][basic] = new cage(eagles.get(counter));
          cages[1][basic] = new cage(eagles.get(counter+1));
        }
      }
      System.out.println("\nCage arrangement:");
      System.out.println("location: outdoor");
      System.out.print("level 3: ");
      if(cages[2]!=null){
        for(int a =0; a<cages[2].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[2][a].name,cages[2][a].animalLength,cages[2][a].type);
        }
      }
      System.out.print("\nlevel 2: ");
      if(cages[1]!=null){
        for(int a =0; a<cages[1].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[1][a].name,cages[1][a].animalLength,cages[1][a].type);
        }
      }
      System.out.print("\nlevel 1: ");
      if(cages[0]!=null){
        for(int a =0; a<cages[0].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[0][a].name,cages[0][a].animalLength,cages[0][a].type);
        }
      }
    }
}
  public static void cagesarrangehamster(cage[][] cages, ArrayList<hamster> hamsters){
    if(!hamsters.isEmpty()){
      int counter=0;
      int basic = hamsters.size()/3;
      int rem = hamsters.size()%3;
      if(hamsters.size()<3){
        if(hamsters.size()==2){
          cages[0]= new cage[1];cages[1]=new cage[1];
          cages[0][0] = new cage(hamsters.get(0));
          cages[1][0] = new cage(hamsters.get(1));
        }
        if(hamsters.size()==1){
          cages[0]=new cage[1];
          cages[0][0] = new cage(hamsters.get(0));
        }
      }
      else{
        if(rem==0){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic];
        }
        else if(rem==1){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic+1];
        }
        else{
          cages[0]= new cage[basic];
          cages[1]= new cage[basic+1];
          cages[2]= new cage[basic+1];
        }
        for(int a =0; a<3;a++){
          for(int b =0;b<basic;b++){
            cages[a][b]=new cage(hamsters.get(counter));
            counter+=1;
          }
        }
        if(rem==1){
          cages[2][basic] = new cage(hamsters.get(counter));
        }
        if(rem==2){
          cages[2][basic] = new cage(hamsters.get(counter));
          cages[1][basic] = new cage(hamsters.get(counter+1));
        }
      }
      System.out.println("\nCage arrangement:");
      System.out.println("location: indoor");
      System.out.print("level 3: ");
      if(cages[2]!=null){
        for(int a =0; a<cages[2].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[2][a].name,cages[2][a].animalLength,cages[2][a].type);
        }
      }
      System.out.print("\nlevel 2: ");
      if(cages[1]!=null){
        for(int a =0; a<cages[1].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[1][a].name,cages[1][a].animalLength,cages[1][a].type);
        }
      }
      System.out.print("\nlevel 1: ");
      if(cages[0]!=null){
        for(int a =0; a<cages[0].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[0][a].name,cages[0][a].animalLength,cages[0][a].type);
        }
      }
    }
}
  public static void cagesarrangeparrot(cage[][] cages, ArrayList<parrot> parrots){
    if(!parrots.isEmpty()){
      int counter=0;
      int basic = parrots.size()/3;
      int rem = parrots.size()%3;
      if(parrots.size()<3){
        if(parrots.size()==2){
          cages[0]= new cage[1];cages[1]=new cage[1];
          cages[0][0] = new cage(parrots.get(0));
          cages[1][0] = new cage(parrots.get(1));
        }
        if(parrots.size()==1){
          cages[0]=new cage[1];
          cages[0][0] = new cage(parrots.get(0));
        }
      }
      else{
        if(rem==0){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic];
        }
        else if(rem==1){
          cages[0]= new cage[basic];
          cages[1]= new cage[basic];
          cages[2]= new cage[basic+1];
        }
        else{
          cages[0]= new cage[basic];
          cages[1]= new cage[basic+1];
          cages[2]= new cage[basic+1];
        }
        for(int a =0; a<3;a++){
          for(int b =0;b<basic;b++){
            cages[a][b]=new cage(parrots.get(counter));
            counter+=1;
          }
        }
        if(rem==1){
          cages[2][basic] = new cage(parrots.get(counter));
        }
        if(rem==2){
          cages[2][basic] = new cage(parrots.get(counter));
          cages[1][basic] = new cage(parrots.get(counter+1));
        }
      }
      System.out.println("\nCage arrangement:");
      System.out.println("location: indoor");
      System.out.print("level 3: ");
      if(cages[2]!=null){
        for(int a =0; a<cages[2].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[2][a].name,cages[2][a].animalLength,cages[2][a].type);
        }
      }
      System.out.print("\nlevel 2: ");
      if(cages[1]!=null){
        for(int a =0; a<cages[1].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[1][a].name,cages[1][a].animalLength,cages[1][a].type);
        }
      }
      System.out.print("\nlevel 1: ");
      if(cages[0]!=null){
        for(int a =0; a<cages[0].length;a++){
          System.out.printf( "%s (%d - %s), ",cages[0][a].name,cages[0][a].animalLength,cages[0][a].type);
        }
      }
    }
  }
}
