import java.util.*;

public class JavariPark{


  public static void main(String args[]){
    cage[][] cagescat = new cage[3][];
    cage[][] cageslion= new cage[3][];
    cage[][] cageshamster= new cage[3][];
    cage[][] cageseagle= new cage[3][];
    cage[][] cagesparrot= new cage[3][];
    int tempint;
    String tempstring;
    Scanner scan = new Scanner (System.in);
    String[] type = {"cat","lion","eagle","parrot","hamster"};
    System.out.println("Welcome to Javari Park!");
    System.out.println("Input the number of animals");
    for(int a = 0; a<5;a++){
      System.out.print(type[a]+": ");
      tempint = Integer.parseInt(scan.nextLine());
      if(tempint==0){
        continue;
      }
      else{
        System.out.printf("Provide the information of %s(s)\n",type[a]);
        tempstring = scan.nextLine();
        for(String before : tempstring.split(",")){
          String[]after = before.split("\\|");
              if(type[a].equals("cat")){
                new cat(after[0],Integer.parseInt(after[1]))  ;
              }
              if(type[a].equals("lion")){
                new lion(after[0],Integer.parseInt(after[1]));
              }
              if(type[a].equals("eagle")){
                new eagle(after[0],Integer.parseInt(after[1]));
              }
              if(type[a].equals("parrot")){
                new parrot(after[0],Integer.parseInt(after[1]));
              }
              if(type[a].equals("hamster")){
                new hamster(after[0],Integer.parseInt(after[1]));
              }
        }
    }
  }
  System.out.println("Animals have been successfully recorded");
  System.out.println();
  System.out.println("=============================================");
  arrange.cagesarrangecat(cagescat, cat.list);
  arrange.rearrange(cagescat);
  arrange.cagesarrangelion(cageslion, lion.list);
  arrange.rearrange(cageslion);
  arrange.cagesarrangehamster(cageshamster, hamster.list);
  arrange.rearrange(cageshamster);
  arrange.cagesarrangeeagle(cageseagle, eagle.list);
  arrange.rearrange(cageseagle);
  arrange.cagesarrangeparrot(cagesparrot, parrot.list);
  arrange.rearrange(cagesparrot);
  System.out.println("\nNUMBER OF ANIMALS:");
  System.out.printf("cat:%d\n",cat.list.size());
  System.out.printf("lion:%d\n",lion.list.size());
  System.out.printf("parrot:%d\n",parrot.list.size());
  System.out.printf("eagle:%d\n",eagle.list.size());
  System.out.printf("hamster:%d\n",hamster.list.size());
  System.out.println("\n=============================================");
  boolean con = true;
  while(con){
    String name;
    System.out.println("Which animal you want to visit?");
    System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
    int choice = Integer.parseInt(scan.nextLine());
    outerloop:
    switch(choice){
      case 1:
        System.out.print("Mention the name of cat you want to visit: ");
        name = scan.nextLine();
        for(cat cats : cat.list){
          if(cats.getName().equals(name)){
            System.out.printf("You are visiting %s (cat) now, what would you like to do?\n",name);
            System.out.println("1: Brush the fur 2: Cuddle");
            switch(Integer.parseInt(scan.nextLine())){
              case 1:
                cats.brushed();
                break;
              case 2:
                cats.cuddled();
                break;
            }
            break outerloop;
          }
        }
        System.out.println("There is no cat with that name! Back to the office!\n");
        break;
      case 2:
      System.out.print("Mention the name of eagle you want to visit: ");
      name = scan.nextLine();
      for(eagle eagles : eagle.list){
        if(eagles.getName().equals(name)){
          System.out.printf("You are visiting %s (eagle) now, what would you like to do?\n",name);
          System.out.println("1: Order to fly");
          switch(Integer.parseInt(scan.nextLine())){
            case 1:
              eagles.fly();
              break;
          }
          break outerloop;
        }
      }
      System.out.println("There is no eagle with that name! Back to the office!\n");
      break;
      case 3:
        System.out.print("Mention the name of hamster you want to visit: ");
        name = scan.nextLine();
        for(hamster hamsters : hamster.list){
          if(hamsters.getName().equals(name)){
            System.out.printf("You are visiting %s (hamster) now, what would you like to do?\n",name);
            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
            switch(Integer.parseInt(scan.nextLine())){
              case 1:
                hamsters.gnaw();
                break;
              case 2:
                hamsters.run();
                break;
            }
            break outerloop;
          }
        }
        System.out.println("There is no hamster with that name! Back to the office!\n");
        break;
        case 4:
          System.out.print("Mention the name of parrot you want to visit: ");
          name = scan.nextLine();
          for(parrot parrots : parrot.list){
            if(parrots.getName().equals(name)){
              System.out.printf("You are visiting %s (parrot) now, what would you like to do?\n",name);
              System.out.println("1: Order to fly 2: Do conversation");
              switch(Integer.parseInt(scan.nextLine())){
                case 1:
                  parrots.fly();
                  break;
                case 2:
                  parrots.imitate(scan.nextLine());
                  break;
                }
                break outerloop;
            }
          }
          System.out.println("There is no parrot with that name! Back to the office!\n");
          break;
          case 5:
            System.out.print("Mention the name of lion you want to visit: ");
            name = scan.nextLine();
            for(lion lions : lion.list){
              if(lions.getName().equals(name)){
                System.out.printf("You are visiting %s (lion) now, what would you like to do?\n",name);
                System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                switch(Integer.parseInt(scan.nextLine())){
                  case 1:
                    lions.hunt();
                    break;
                  case 2:
                    lions.groomed();
                    break;
                  case 3:
                    lions.disturbed();
                    break;
                }
                break outerloop;
              }
            }
            System.out.println("There is no lion with that name! Back to the office!\n");
            break;
            case 99:
              con=false;
              break;
    }
  }
    }
  }
