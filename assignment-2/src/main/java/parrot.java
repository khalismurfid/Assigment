import java.util.ArrayList;
public class parrot extends animals{
  int length;
  public static ArrayList<parrot> list = new ArrayList <parrot>();

  public parrot(String name, int length){
    super(name,length);
    list.add(this);
  }

  public void fly(){
    System.out.printf("Parrot %s flies!\n",name);
    System.out.println(name+ " makes a voice: FLYYYY…");
    System.out.println("Back to the office!\n");
  }

  public void imitate(String speech){
    System.out.println(name+ " says: "+speech.toUpperCase());
    System.out.println("Back to the office!\n");
  }

  public void mumble(){
    System.out.println(name+ " makes a voice: HM?");
    System.out.println("Back to the office!\n");
  }

}
