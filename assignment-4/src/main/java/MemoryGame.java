import javax.swing.*;


public class MemoryGame {
    public MemoryGame(){
        JFrame frame = new JFrame("Memory Game by Khalis");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 1280);
        Board board = new Board();
        frame.getContentPane().add(board.getPanel());
        frame.setResizable(true);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MemoryGame game = new MemoryGame();
            }
        });
    }
}
