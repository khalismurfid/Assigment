package card;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
* Class that represent Card.
*/

public class Card{

private int id;
private JButton button = new JButton();
private ImageIcon img;
private ImageIcon cover =  new ImageIcon("D:\\TP\\Assigment\\assignment-4\\src\\main\\java\\images\\cover.jpg");
private boolean isMatched;

/**
* Constructor of Card.
* @param id id of the card
* @param ImageIcon image behind the card
*/

public Card (int id ,ImageIcon img){
  this.id = id;
  this.img = img;
  button.setPreferredSize(new Dimension(100,150));
  flop();
}
/**
* Close the card
*/

public void flop(){
  button.setIcon(cover);
}

/**
* open the card
*/

public void flip(){
  button.setIcon(img);
}

public int getId(){
  return id;
}
public JButton getButton(){
  return button;
}
public boolean getisMatched(){
  return isMatched;
}
public void setisMatched(boolean isMatched){
  this.isMatched = isMatched;
}

public boolean equals(Card card){
  return id ==card.getId();
}

}
